package io.swagger.petstore;

import io.qameta.allure.Description;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.swagger.petstore.models.pet.PetModel;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.hamcrest.Matchers;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class TestPet {

    private String baseURI = "http://petstore.swagger.io/";

//    @Test
    public void petTest01() {
        String idTestValue = RandomStringUtils.randomNumeric(6);
        RestAssured.given()
                .baseUri(baseURI)
                .basePath("v2/pet")
                .contentType(ContentType.JSON)
                .header("api_key", "chr255")
                .body("{\n" +
                        "  \"id\": " + idTestValue + ",\n" +
                        "  \"name\": \"" + "Tim" + "\",\n" +
                        "  \"photoUrls\": [],\n" +
                        "  \"tags\": [],\n" +
                        "  \"status\": \"pending\"\n" +
                        "}")
                .when().post()
                .then()
                .body("id", Matchers.equalTo(Integer.valueOf(idTestValue)))
                .extract().response()
                .prettyPrint();
    }


    String petID = "1";

    @Test
    @Description("Test Description: Login test with wrong username and wrong password.")
    public void petTest02() {
        RestAssured.given()
                .log().all()
                .baseUri(baseURI)
                .basePath("v2/pet/" + petID)
                .contentType(ContentType.JSON)
                .header("api_key", "chr255")
                .when().get()
                .then()
                .body("id", Matchers.equalTo(Integer.valueOf(petID)))
                .extract().response()
                .prettyPrint();
    }

    @Test
    @Description("GET ")
    public void petTest03() {

        List<String> petStatuses = new ArrayList<String>();
        petStatuses.add("available");
        petStatuses.add("pending");
        petStatuses.add("sold");

        int i = 1;
        for (String status : petStatuses) {
            System.out.printf("Test %d.\n", i);

            Response response = RestAssured.given()
                    .baseUri(baseURI)
                    .basePath("v2/pet/findByStatus")
                    .contentType(ContentType.JSON)
                    .header("api_key", "chr255")
                    .param("status", status)
                    .when().get();

            response
                    .then()
                    .statusCode(200);
            System.out.println("Response: " + response.asString());
            i++;
        }
    }

    @Test
    @Description("Test Description: Login test with wrong username and wrong password.")
    public void petTest04() {
//        String idTestValue = RandomStringUtils.(6);
        String testPetName = RandomStringUtils.randomAlphabetic(6);
        int idTestValue = RandomUtils.nextInt(0, 9000);

        PetModel pet = new PetModel(idTestValue, null, testPetName, null, null, "AVAILABLE");

//        RestAssured.requestSpecification = new RequestSpecBuilder()
//                .setContentType(ContentType.JSON)
//                .addHeader("api_key", "chr255")
//                .build();


        RequestSpecBuilder requestSpecification = new RequestSpecBuilder()
                .setBaseUri(baseURI)
                .setContentType(ContentType.JSON)
                .addHeader("api_key", "chr255")
                ;

        RestAssured.given(requestSpecification.build())
                .log().all()
//                .baseUri(baseURI)
                .basePath("v2/pet")
//                .contentType(ContentType.JSON)
//                .header("api_key", "chr255")
                .body(pet)
                .when().post()
                .then()
                .body(Matchers.equalTo(pet))
                .extract().response()
                .prettyPrint();
    }
}
