package io.swagger.petstore;

import io.swagger.petstore.controllers.pet.PetCtr;
import io.swagger.petstore.models.pet.PetModel;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.junit.Test;

public class TestObjectStyle {

    @Test
    public void Test(){
        String testPetName = RandomStringUtils.randomAlphabetic(6);
        int idTestValue = RandomUtils.nextInt(0, 9000);

        PetModel testPet = new PetModel(idTestValue, null, testPetName, null, null, "AVAILABLE");

        PetModel pet = new PetCtr().addNewPet(testPet);
    }
}
