package io.swagger.petstore.controllers;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;

public abstract class AbstractController {

    private static String baseURI = "http://petstore.swagger.io";

    static {

        RestAssured.requestSpecification = new RequestSpecBuilder()
                .setBaseUri(baseURI)
                .addHeader("api_key", "chr255")
                .setContentType(ContentType.JSON)
                .log(LogDetail.ALL).build();
    }

}
