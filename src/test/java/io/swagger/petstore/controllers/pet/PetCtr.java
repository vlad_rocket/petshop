package io.swagger.petstore.controllers.pet;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.swagger.petstore.controllers.AbstractController;
import io.swagger.petstore.models.pet.PetModel;

import static io.restassured.RestAssured.given;

public class PetCtr extends AbstractController{

    public PetCtr(){

        RestAssured.requestSpecification = new RequestSpecBuilder()
                .setBaseUri("http://petstore.swagger.io")
                .setBasePath("/v2/pet")
                .log(LogDetail.ALL).build();
    }

    public  PetModel addNewPet(PetModel pet){
        return given()

                .body(pet)
                .when()
                .post()
//                .then()
//                .extract()
                .as(PetModel.class);
    }
}
